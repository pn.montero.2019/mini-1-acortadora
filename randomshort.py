#!/usr/bin/env python3
import socket
import shelve
import random
import string

class webApp:
    def parse(self, request):
        # Dividimos la solicitud en líneas
        lines = request.split('\r\n')
        # La primera línea es la línea de estado, que contiene el método y la URL
        status_line = lines[0]
        method, url, _ = status_line.split()
        # El cuerpo (si existe) está después de una línea en blanco
        body = '\r\n'.join(lines[lines.index('') + 1:])
        return {'method': method, 'url': url, 'body': body}

    def process(self, parsed_request):
        # Procesamos la solicitud en función del método
        if parsed_request['method'] == 'GET':
            return self.process_get(parsed_request)
        elif parsed_request['method'] == 'POST':
            return self.process_post(parsed_request)
        else:
            # Manejamos las solicitudes desconocidas
            return ("404 Bad Request", "<html><body><h1>Bad Request</h1></body></html>")

    def process_get(self, parsed_request):
        # Procesamos las solicitudes GET
        url = parsed_request['url']
        if url == '/':
            return self.create_url_form()
        elif url in self.get_shortened_urls():
            # Redireccionamos a la URL real
            real_url = self.urls_db[url]
            return ("302 Found", f"<html><body>Redirecting to <a href='{real_url}'>{real_url}</a></body></html>")
        else:
            # Manejamos la URL desconocida
            return ("404 Not Found", "<html><body><h1>Not Found</h1></body></html>")

    def create_url_form(self):
        # Generamos el formulario HTML para acortar URL
        html = "<html><body>"
        html += "<h1>URL Shortener</h1>"
        html += "<form method='POST'>"
        html += "<input type='text' name='url'>"
        html += "<button type='submit'>Shorten</button>"
        html += "</form>"
        html += "<h2>Shortened URLs:</h2>"
        html += "<ul>"
        # Mostramos la lista de URLs acortadas
        for short_url in self.get_shortened_urls():
            html += f"<li>{short_url}</li>"
        html += "</ul>"
        html += "</body></html>"
        return ("200 OK", html)

    def process_post(self, parsed_request):
        # Procesamos las solicitudes POST
        url = parsed_request['body']
        if not (url.startswith("http://") or url.startswith("https://")):
            url = "https://" + url  # Añadimos https:// si no está presente
        short_url = self.shorten_url(url)
        self.urls_db[short_url] = url
        self.urls_db.sync()  # Guardamos los cambios en la base de datos
        return (
            "200 OK",
            f"<html><body><h1>Shortened URL: <a href='{short_url}'>{short_url}</a></h1></body></html>"
        )

    def shorten_url(self, url):
        # Generamos un nombre aleatorio para la URL acortada
        characters = string.ascii_letters + string.digits
        return '/' + ''.join(random.choice(characters) for _ in range(self.random_length))

    def get_shortened_urls(self):
        # Obtiene todas las URLs acortadas almacenadas en la base de datos
        return list(self.urls_db.keys())

    def __init__(self, hostname, port):
        # Inicialización de la aplicación web
        self.hostname = hostname
        self.port = port
        self.random_length = 6  # Longitud del nombre aleatorio para las URL acortadas
        self.urls_db = shelve.open("urls.db", writeback=True)  # Base de datos para las URL acortadas

        # Definimos el socket TCP y enlace a un puerto
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.mySocket.bind((hostname, port))
        self.mySocket.listen(5)  # Espera una cola de máximo 5 conexiones

        # Bucle principal para manejar conexiones entrantes
        try:
            while True:
                print("Waiting for connections")
                recvSocket, address = self.mySocket.accept()
                print("HTTP request received (going to parse and process):")
                request = recvSocket.recv(2048).decode('utf-8')
                print(request)
                parsed_request = self.parse(request)
                return_code, html_answer = self.process(parsed_request)
                print("Answering back...")
                response = f"HTTP/1.1 {return_code}\r\n\r\n{html_answer}\r\n"
                recvSocket.send(response.encode('utf-8'))
                recvSocket.close()
        except KeyboardInterrupt:
            print("Closing server socket")
            self.mySocket.close()

if __name__ == "__main__":
    testWebApp = webApp("localhost", 1234)
